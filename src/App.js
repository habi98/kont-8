import React, { Component,Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/Header/Header";
import {BrowserRouter, Route, } from "react-router-dom";
import Container from "reactstrap/es/Container";
import QuotesLink from "./containers/QuotesLink/QuotesLink";
import AddQuote from "./containers/AddQuote/AddQuote";


class App extends Component {
  render() {
    return (
        <BrowserRouter>

          <Fragment>
              <Header/>
            <Container>
                <Route path="/" exact component={QuotesLink}/>
                <Route path="/add-quote" exact component={AddQuote}/>
                <Route path="/quotes/:quoteId" exact component={QuotesLink}/>
            </Container>
          </Fragment>

        </BrowserRouter>

    );
  }
}

export default App;
