import React from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLInk} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <Navbar color="dark" dark expand="md">
                <NavbarBrand href="/">Quotes Central</NavbarBrand>
                <NavbarToggler  />
                <Collapse navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouterNavLInk} to="/">Quotes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLInk} to="/add-quote">submit new quote</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
};

export default Header;