import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {COTEGORIES} from "../../cotegories";


class QuoteForm extends Component {
    state = {
        author: '',
        quotes: '',
        category: Object.keys(COTEGORIES)[0]
    };

    valueChanged = (event) => {
        const name = event.target.name ;
       this.setState({[name]: event.target.value})
    };

    submitHandler = e => {
        e.preventDefault();
        this.props.onSubmit({...this.state})
    };

    render() {
        return (
            <Form className="ProductForm" mrt={10} onSubmit={this.submitHandler}>
                <h1>Submit new quote</h1>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" value={this.state.category} onChange={this.valueChanged} name="category" id="cotegory"
                              >
                            {Object.keys(COTEGORIES).map(categoryId => (
                                <option key={categoryId} value={categoryId}>{COTEGORIES[categoryId]}</option>
                            ))}


                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Author</Label>
                    <Col sm={10}>
                        <Input type="text" name="author" id="name"
                            value={this.state.author} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Quote text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="quotes"
                               value={this.state.quotes} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuoteForm;