import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody, CardColumns,
    CardFooter, CardHeader,
    CardText,
    CardTitle,
    Col,
    Nav,
    NavItem,
    NavLink,
    Row
} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";
import {COTEGORIES} from "../../cotegories";
import axios from '../../axios-quotes';

class QuotesLink extends Component {
    state = {
      quotes: null,
    };


    loadData = () => {
        let url = "quotes.json";

        const category = this.props.match.params.quoteId;
        if(category) {
            url += `?orderBy="category"&equalTo="${category}"`
        }
        axios.get(url).then(response => {
            if(response.data) {
                console.log(response.data);
                const quotes = Object.keys(response.data).map( id => {
                    return {...response.data[id], id}
                });
                this.setState({quotes})

            }
        })
    };

    componentDidMount() {
       this.loadData()
    }

    componentDidUpdate(prevProps) {
        if(this.props.match.params.quoteId && this.props.match.params.quoteId !== prevProps.match.params.quoteId) {
            this.loadData()
        }
    }
    deleteQuote = (id) => {
        axios.delete("/quotes/" + id + ".json", ).then(() => {
            let url = "quotes.json";

            const category = this.props.match.params.quoteId;
            if(category) {
                url += `?orderBy="category"&equalTo="${category}"`
            }
            axios.get(url).then(response => {
                if(response.data) {
                    const quotes = Object.keys(response.data).map( id => {
                        return {...response.data[id], id}
                    });
                    this.setState({quotes})

                } else  this.setState({quotes: null})
            });
            this.props.history.replace('/')
        })


    };


    render() {
        let quotes = null;
        if(this.state.quotes) {
            quotes = this.state.quotes.map(quote => (
                <Card key={quote.id}>
                    <CardHeader>Author: {quote.author}</CardHeader>
                    <CardBody>
                        <CardTitle>quotes: {quote.quotes}</CardTitle>
                        <CardText>category: {quote.category}</CardText>
                    </CardBody>
                    <CardFooter>

                        <RouterNavLink to={'/products/' + quote.id + '/edit'}>
                            <Button >Edit</Button>
                        </RouterNavLink>
                        <Button onClick={() => this.deleteQuote(quote.id)}>delete</Button>
                    </CardFooter>
                </Card>
            ))
        }
        return (
            <div>
                <Row>
                    <Col>
                <Nav vertical>
                    <NavItem >
                        <NavLink tag={RouterNavLink} to="/" exact >All quotes</NavLink>
                    </NavItem>
                    {Object.keys(COTEGORIES).map(quotesId => (
                        <NavItem key={quotesId}>
                            <NavLink exact tag={RouterNavLink} to={"/quotes/" + quotesId}>{COTEGORIES[quotesId]}</NavLink>
                        </NavItem>
                    ))}
                </Nav>
                    </Col>
                    <Col sm={9}>
                        <CardColumns>
                            {quotes}
                        </CardColumns>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default QuotesLink;