import React, {Component} from 'react';
import QuoteForm from "../QuoteForm/QuoteForm";
import axios from '../../axios-quotes';
// import {COTEGORIES} from "../../cotegories";

class AddQuote extends Component {

    addQuote = quote => {
        axios.post("/quotes.json", quote).then(() => {
            this.props.history.push('/')
        })
    };

    render() {
        return (
            <div>
               <QuoteForm  onSubmit={this.addQuote}/>
            </div>
        );
    }
}

export default AddQuote;